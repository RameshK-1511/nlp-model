FROM ubuntu:20.04

COPY . /app
WORKDIR /app

EXPOSE 4000
RUN apt-get update && apt-get install -y python3-pip
# RUN pip install --upgrade pip
RUN pip3 install -r requirements.txt
RUN python3 -m nltk.downloader punkt

ENTRYPOINT  ["python3"]

CMD ["app.py"]